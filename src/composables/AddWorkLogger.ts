import { ref, reactive } from "vue";

export interface WorkLog {
  id: number;
  duration: number;
  project: string;
  remark: string;
  isEditing: boolean;
}

export const addWorkLoggerComposable = () => {
  let workLogId = 0;
  const duration = ref(0);
  const project = ref("");
  const remark = ref("");
  const workLoggerList = reactive<WorkLog[]>([]);
  const projectList = reactive(["Hydra", "Dragon MY", "Dragon P2P"]);

  const addLog = () => {
    if (!duration.value || !project.value || !remark.value) {
      alert("Please fill all the form inputs!");
      return;
    }

    workLoggerList.push({
      id: workLogId,
      duration: duration.value,
      project: project.value,
      remark: remark.value,
      isEditing: false,
    });

    workLogId++;
    console.log(workLoggerList);
  };

  return {
    workLoggerList,
    projectList,
    duration,
    project,
    remark,
    addLog,
  };
};
