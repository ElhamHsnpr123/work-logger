import { ref, computed } from "vue";
import { addWorkLoggerComposable, WorkLog } from "./AddWorkLogger";

export const workLoggerListComposable = (workLoggerList: WorkLog[]) => {
  const durationToEdit = ref(0);
  const projectToEdit = ref("");
  const remarkToEdit = ref("");
  const currentSortType = ref("duration");
  const currentSortDir = ref(1);
  const isSorted = ref(false);
  const toBeShownListLength = ref(4);
  const buttonTextDuration = ref("Duration");
  const buttonTextProject = ref("Project");

  const sorted = (s: string) => {
    if (s === currentSortType.value) {
      currentSortDir.value = currentSortDir.value *= -1;
    }

    if (currentSortDir.value == 1) {
      if (s === "duration") {
        buttonTextDuration.value = "Duration ^";
        buttonTextProject.value = "Project";
      } else if (s === "project") {
        buttonTextDuration.value = "Duration";
        buttonTextProject.value = "Project ^";
      }
    } else {
      if (s === "duration") {
        buttonTextDuration.value = "Duration v";
        buttonTextProject.value = "Project";
      } else if (s === "project") {
        buttonTextDuration.value = "Duration";
        buttonTextProject.value = "Project v";
      }
    }

    currentSortType.value = s;
    isSorted.value = true;
  };

  const realWorkLoggerList = computed(() => {
    const sortedWorkLoggerList = workLoggerList
      .slice()
      .sort((a: WorkLog, b: WorkLog) => {
        if (currentSortType.value === "duration") {
          if (Number(a.duration) < Number(b.duration)) {
            return -1 * currentSortDir.value;
          }
          if (Number(a.duration) > Number(b.duration)) {
            return 1 * currentSortDir.value;
          }
        } else if (currentSortType.value === "project") {
          if (a.project < b.project) return -1 * currentSortDir.value;
          if (a.project > b.project) return 1 * currentSortDir.value;
        }
        return 0;
      });
    return isSorted.value ? sortedWorkLoggerList : workLoggerList;
  });

  const reset = () => {
    isSorted.value = false;
    currentSortDir.value = 1;
    buttonTextDuration.value = "Duration";
    buttonTextProject.value = "Project";
  };

  const realToBeShownListLength = computed(() => {
    return Math.min(workLoggerList.length, toBeShownListLength.value);
  });

  const showShowMoreButton = computed(() => {
    return (
      workLoggerList.length > 4 &&
      toBeShownListLength.value < workLoggerList.length
    );
  });

  const showMore = () => {
    toBeShownListLength.value += 4;
  };

  const deleteLog = (id: number) => {
    for (let index = 0; index < workLoggerList.length; index++) {
      if (workLoggerList[index].id == id) {
        workLoggerList.splice(index, 1);
      }
    }
  };
  const editLog = (id: number) => {
    for (const workLog of workLoggerList) {
      if (workLog.id == id) {
        workLog.isEditing = true;
        durationToEdit.value = workLog.duration;
        projectToEdit.value = workLog.project;
        remarkToEdit.value = workLog.remark;
      } else {
        workLog.isEditing = false;
      }
    }
  };
  const saveLog = (id: number) => {
    if (!durationToEdit.value || !projectToEdit.value || !remarkToEdit.value) {
      alert("Please fill all the form inputs!");
      return;
    }

    for (const workLog of workLoggerList) {
      if (workLog.id == id) {
        workLog.duration = durationToEdit.value;
        workLog.project = projectToEdit.value;
        workLog.remark = remarkToEdit.value;
        workLog.isEditing = false;
        break;
      }
    }
  };

  return {
    sorted,
    realWorkLoggerList,
    reset,
    showShowMoreButton,
    showMore,
    toBeShownListLength,
    realToBeShownListLength,
    editLog,
    saveLog,
    deleteLog,
    durationToEdit,
    projectToEdit,
    remarkToEdit,
    buttonTextDuration,
    buttonTextProject,
  };
};
