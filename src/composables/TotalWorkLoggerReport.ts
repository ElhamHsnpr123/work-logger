import { computed } from "vue";
import { WorkLog } from "./AddWorkLogger";

export const totalWorkLoggerComposale = (
  workLoggerList: WorkLog[],
  projectList: string[]
) => {
  const totalWorkHours = computed(() => {
    return workLoggerList.reduce(
      (acc, item) => Number(acc) + Number(item.duration),
      0
    );
  });

  const perProjectHours = computed(() => {
    const tempPerProjectHours: number[] = [];
    for (const projectName of projectList) {
      const workLogsForCurrentProject = workLoggerList.filter(
        (s) => s.project === projectName
      );
      tempPerProjectHours.push(
        workLogsForCurrentProject.reduce(
          (acc, item) => Number(acc) + Number(item.duration),
          0
        )
      );
    }

    return tempPerProjectHours;
  });

  return { totalWorkHours, perProjectHours };
};
