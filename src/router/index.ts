import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import WorkLogger from "../components/WorkLogger.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "WorkLogger",
    component: WorkLogger,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
